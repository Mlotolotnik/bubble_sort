##funkcja sortująca

def bubble_sort(to_sort):
    result = to_sort
    #print(result)
    sort_iterations_left = len(result) -1
    sort_incomplete = True
    if len(result) > 1:
        while sort_incomplete:
            sort_incomplete = False
            for i in range(sort_iterations_left):
                if result[i] > result[i+1]:
                    temp = result[i]
                    result[i] = result[i+1]
                    result[i + 1] = temp
                    sort_incomplete = True
            sort_iterations_left -= 1
            #print(result)
    return result

##pobranie z pliku ciągów do posortowania
to_sort = open("to_sort.txt")
raw_string = to_sort.read()
to_sort.close()

print("Przed sortowaniem:")
print(raw_string)

##przełożenie danych ze string do ciągów int
raw_string = raw_string.replace("(","")
raw_string = raw_string.replace(")","")
list_of_series_to_sort = raw_string.splitlines()
list_of_series = []

for i in list_of_series_to_sort:
    list_of_series.append(i.split(","))

for i in range(len(list_of_series)):
    for k in range(len(list_of_series[i])):
        try:
            list_of_series[i][k] = int(list_of_series[i][k])
        except ValueError:
            list_of_series[i][k] = None

#print("Przed sortowaniem:")
#print(list_of_series)

##sortowanie
result = []
for i in list_of_series:
    result.append(bubble_sort(i))

#print("Po sortowaniu:")
#print(result)

##przełożenie wyników z ciągów int do string
for i in range(len(result)):
    for k in range(len(result[i])):
        try:
            result[i][k] = str(result[i][k])
        except ValueError:
            result[i][k] = None

result_str = ""

for i in range(len(result)):
    result_str = result_str + "("
    for k in range(len(result[i])):
        if result[i][k] == "None":
            result_str = result_str + ""
        else:
            result_str = result_str + (result[i][k])
        if k != len(result[i]) - 1:
            result_str = result_str + ","        
    result_str = result_str + ")\n"

##wysłanie wyników do pliku
result = result_str
print("Po sortowaniu:")
print(result)

file = open("result.txt", "w")
file.write(result)
file.close()

